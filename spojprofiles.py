import urllib
import re
import urllib.request
import csv
import pymongo

'''
Requires csvfile with spoj usernames to run the code.



'''

def profiles_mongo():

    mng_client = pymongo.MongoClient('localhost', 27017)
    mng_db = mng_client['SPOJ']
    collection_name = 'profiles_Spoj'

    # if collection_name in mng_db.collection_names():
        # mng_db.drop_collection(collection_name)

    db_cm = mng_db[collection_name]

    for cont in profiles_spoj('users.csv'):
        db_cm.insert_one(cont)



def profiles_spoj(csvfile):
    with open(csvfile, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='|')
        for row in reader :
            # print(row)
            url = "http://www.spoj.com/users/" + row[0] +  "/"
            try :
                htmlfile = urllib.request.urlopen(url)
                htmltext = htmlfile.read()
                strtext = htmltext.decode('utf-8')
            except urllib.error.HTTPError:
                continue

            location_regex = 'map-marker\"></i>(.+?)</p>'
            Name_regex = '<h3>(.+?)</h3>\t\t<h4>'
            image_regex = 'src=\"(.+?)\" class'
            Institute_regex = 'Institution: (.+?)</p>'
            rank_regex = 'World Rank: #(.+?)\('
            point_regex = 'World Rank: #.+\((.+?) points'
            solution_regex = 'Solutions submitted</dt>\n\t\t\t\t\t\t\t\t<dd>(.+?)</dd>'
            problems_regex = 'Problems solved</dt>\n\t\t\t\t\t\t\t\t<dd>(.+?)</dd>'

            location_pattern = re.compile(location_regex)
            rank_pattern = re.compile(rank_regex)
            point_pattern = re.compile(point_regex)
            solution_pattern = re.compile(solution_regex)
            problems_pattern = re.compile(problems_regex)
            Institute_pattern = re.compile(Institute_regex)
            Name_pattern = re.compile(Name_regex)
            image_pattern = re.compile(image_regex)


            # Add_to_row(Name_pattern,strtext,row)
            Name = re.findall(Name_pattern,strtext)
            if len(Name)==0:
                row.extend([""])
            else:
                row.append(Name[0].strip())

            Name = re.findall(location_pattern,strtext)
            if len(Name)==0:
                row.extend([""])
            else:
                row.append(Name[0].strip())

            Name = re.findall(rank_pattern,strtext)
            if len(Name)==0:
                row.extend([""])
            else:
                row.append(int(Name[0].strip()))

            Name = re.findall(point_pattern,strtext)
            if len(Name)==0:
                row.extend([""])
            else:
                row.append(float(Name[0].strip()))

            Name = re.findall(problems_pattern,strtext)
            if len(Name)==0:
                row.extend([""])
            else:
                row.append(int(Name[0].strip()))

            Name = re.findall(solution_pattern,strtext)
            if len(Name)==0:
                row.extend([""])
            else:
                row.append(int(Name[0].strip()))

            Name = re.findall(Institute_pattern,strtext)
            if len(Name)==0:
                row.extend([""])
            else:
                row.append(Name[0].strip())

            Name = re.findall(image_pattern,strtext)
            if len(Name)==0:
                row.extend([""])
            else:
                row.append(Name[0].strip())

            var = ['username','Name','Location','rank','points','problems_solved','solutions_submitted','Institute','image_url']
            b = dict(zip( var,row))
            yield b


profiles_mongo()

