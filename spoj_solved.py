import urllib
import re
import urllib.request
import csv
import pymongo

'''
Requires csvfile with spoj usernames to run the code.



'''
split_regex = 'TODO list'
created_regex = 'href=\"/problems/(.+?)/\"'
Joined_regex = '</i> Joined(.+?)</p>'
problem_regex = 'href=\"/status/(.+?),'


def profiles_mongo():

    mng_client = pymongo.MongoClient('localhost', 27017)
    mng_db = mng_client['SPOJ']
    collection_name = 'problems_Spoj'

    #if collection_name in mng_db.collection_names():
       #  mng_db.drop_collection(collection_name)

    db_cm = mng_db[collection_name]

    for cont in profiles_spoj('users.csv'):
        db_cm.insert_one(cont)
        print('ok')



def profiles_spoj(csvfile):
    with open(csvfile, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='|')
        for row in reader :
            print(row)
            url = "http://www.spoj.com/users/" + row[0] +  "/"
            try :
                htmlfile = urllib.request.urlopen(url)
                htmltext = htmlfile.read()
                strtext = htmltext.decode('utf-8')
            except urllib.error.HTTPError:
                continue


            texts = re.compile("TODO list").split(strtext)
            solved_problems= re.findall(re.compile(problem_regex),texts[0])
            if len(texts)>1 :
                unsolved_problems= re.findall(re.compile(problem_regex),texts[1])
            else :
                unsolved_problems = []
            created_problems = re.findall(re.compile(created_regex),strtext)
            Joined_Date  = re.findall(re.compile(Joined_regex),strtext)

            # Add_to_row(Name_pattern,strtext,row)
            if len(Joined_Date)==0:
                row.extend([""])
            else:
                row.append(Joined_Date[0].strip())

            A = {}
            for j in range(0,len(solved_problems)):
                B = set()
                problem_url = "http://www.spoj.com/status/" + solved_problems[j] + "," + row[0] +  "/"
                try :
                    prob_htmlfile = urllib.request.urlopen(problem_url)
                    prob_htmltext = prob_htmlfile.read()
                    prob_strtext = prob_htmltext.decode('utf-8')
                except urllib.error.HTTPError:
                    continue

                accepted_regex = '<strong>accepted</strong>'
                accepted_split = re.compile(accepted_regex).split(prob_strtext)
                language_regex = '<span>(.+?)</span>'
                for k in range(1,len(accepted_split)):
                    all = re.findall(re.compile(language_regex),accepted_split[k])
                    B.add(all[0])
                A[solved_problems[j]] = list(B)

            row.append(A)
            row.append(unsolved_problems)
            row.append(created_problems)

            #print(row)
            var = ['username','Joined_Date','Solved_Problems','Unsolved_Problems','Problems_Created']
            b = dict(zip( var,row))
            print(b)
            yield b


profiles_mongo()

